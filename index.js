const express = require('express')
const bodyParser = require('body-parser')

const lib = require('./lib')

const port = 3000
const app = express()
app.use(bodyParser.json())

app.get('/ping', (req, res) => {
    res.send("pong")
});

app.get('/info', (req, res) => {
    res.json({
        version: 'v1',
        project: 'improveASIN',
        service: 'api-amazon',
        language: 'node',
        type: 'api'
    })
})

app.post('/products/fetch-by-asin', async (req, res) => {
    const asin = req.body.asin;
    const product = await lib.fetchProductByAsin(asin);
    const status = 'success';
    
    console.log('Complete Response: \n' + JSON.stringify(product, null, 2));

    const items = product.ItemsResult.Items;
    if (items.length > 0) {
        res.json({
            meta: { status },
            product: items[0]
        });
    } else {
        return res.send(404);
    }
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
})