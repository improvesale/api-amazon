const axios = require('axios');
const fs = require('fs');

const ProductAdvertisingAPIv1 = require('paapi5-nodejs-sdk');


function getApiClient() {
  const defaultClient = ProductAdvertisingAPIv1.ApiClient.instance;
  defaultClient.accessKey = 'AKIAIZVVUX4FFY25CX3A';
  defaultClient.secretKey = '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';
  defaultClient.host = 'webservices.amazon.de';
  defaultClient.region = 'eu-west-1';

  return new ProductAdvertisingAPIv1.DefaultApi();
}

async function fetchProductByAsin(asin) {
  return new Promise((resolve, reject) => {
    const apiClient = getApiClient();
    const itemsIds = [asin];
    const partnerTag = 'zedfyled-21';
    const partnerType = 'Associates';

    const getItemsRequest = new ProductAdvertisingAPIv1.GetItemsRequest(itemsIds, partnerTag, partnerType);
    getItemsRequest['Resources'] = [
      "BrowseNodeInfo.BrowseNodes",
      "BrowseNodeInfo.BrowseNodes.Ancestor",
      "BrowseNodeInfo.BrowseNodes.SalesRank",
      "BrowseNodeInfo.WebsiteSalesRank",
      "CustomerReviews.Count",
      "CustomerReviews.StarRating",
      "Images.Primary.Small",
      "Images.Primary.Medium",
      "Images.Primary.Large",
      "Images.Variants.Small",
      "Images.Variants.Medium",
      "Images.Variants.Large",
      "ItemInfo.ByLineInfo",
      "ItemInfo.ContentInfo",
      "ItemInfo.ContentRating",
      "ItemInfo.Classifications",
      "ItemInfo.ExternalIds",
      "ItemInfo.Features",
      "ItemInfo.ManufactureInfo",
      "ItemInfo.ProductInfo",
      "ItemInfo.TechnicalInfo",
      "ItemInfo.Title",
      "ItemInfo.TradeInInfo",
      "Offers.Listings.Availability.MaxOrderQuantity",
      "Offers.Listings.Availability.Message",
      "Offers.Listings.Availability.MinOrderQuantity",
      "Offers.Listings.Availability.Type",
      "Offers.Listings.Condition",
      "Offers.Listings.Condition.SubCondition",
      "Offers.Listings.DeliveryInfo.IsAmazonFulfilled",
      "Offers.Listings.DeliveryInfo.IsFreeShippingEligible",
      "Offers.Listings.DeliveryInfo.IsPrimeEligible",
      "Offers.Listings.DeliveryInfo.ShippingCharges",
      "Offers.Listings.IsBuyBoxWinner",
      "Offers.Listings.LoyaltyPoints.Points",
      "Offers.Listings.MerchantInfo",
      "Offers.Listings.Price",
      "Offers.Listings.ProgramEligibility.IsPrimeExclusive",
      "Offers.Listings.ProgramEligibility.IsPrimePantry",
      "Offers.Listings.Promotions",
      "Offers.Listings.SavingBasis",
      "Offers.Summaries.HighestPrice",
      "Offers.Summaries.LowestPrice",
      "Offers.Summaries.OfferCount",
      "ParentASIN",
      "RentalOffers.Listings.Availability.MaxOrderQuantity",
      "RentalOffers.Listings.Availability.Message",
      "RentalOffers.Listings.Availability.MinOrderQuantity",
      "RentalOffers.Listings.Availability.Type",
      "RentalOffers.Listings.BasePrice",
      "RentalOffers.Listings.Condition",
      "RentalOffers.Listings.Condition.SubCondition",
      "RentalOffers.Listings.DeliveryInfo.IsAmazonFulfilled",
      "RentalOffers.Listings.DeliveryInfo.IsFreeShippingEligible",
      "RentalOffers.Listings.DeliveryInfo.IsPrimeEligible",
      "RentalOffers.Listings.DeliveryInfo.ShippingCharges",
      "RentalOffers.Listings.MerchantInfo"
    ];

    apiClient.getItems(getItemsRequest, function (error, data, response) {
      if (error) {
        console.log('Error calling PA-API 5.0!');
        reject(error);
      } else {
        console.log('API called successfully.');
        resolve(ProductAdvertisingAPIv1.GetItemsResponse.constructFromObject(data));
      }
    });
  })
}


module.exports = {
  fetchProductByAsin
};